Defaults Plugins
================

Example ``flake8`` extension and ``mypy`` plugin that just enforce some
defaults (while allowing extra options).

``flake8``
----------

Installing the ``toolshed`` package is equivalent to the following ``flake8``
config:

.. code-block:: ini

    [flake8]
    select = E,W,F,I,C
    ignore = W503,E203,E501


``mypy``
--------

Installing the ``toolshed`` package and configuring ``mypy`` with:

.. code-block:: ini

    [mypy]
    plugins = toolshed.mypy_plugin

Is equivalent to:

.. code-block:: ini

    [mypy]
    disallow_untyped_defs = true
    disallow_untyped_calls = true
    disallow_typed_decorators = true
    disallow_incomplete_defs = true
    strict_option = true
