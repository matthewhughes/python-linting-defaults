from textwrap import dedent

import pytest
from flake8.api import legacy as flake8


@pytest.mark.parametrize(
    "content",
    (
        pytest.param(
            """\
            some_list = []
            start = 0
            some_list[start + 4 :]
            """,
            id="E203",
        ),
        pytest.param(
            """\
            foo = "some" + "long" + "expression" + "that" + "is" + "very" + "very" + "long" + "indeed"
            """,
            id="E501",
        ),
    ),
)
def test_ignores_provided_ignores(tmpdir, content):
    file = tmpdir.join("file.py").ensure()
    file.write(dedent(content))

    report = flake8.get_style_guide().input_file(str(file))

    assert report.total_errors == 0


@pytest.mark.parametrize(
    ("content", "rule"),
    (
        (
            """\
            1  + 1
            """,
            "E221",
        ),
        (
            """\
            {"foo": 1}.has_key("foo")
            """,
            "W601",
        ),
        (
            """\
            import unused_module
            """,
            "F401",
        ),
    ),
)
def test_selects_provided_selects(tmpdir, content, rule):
    file = tmpdir.join("file.py").ensure()
    file.write(dedent(content))

    report = flake8.get_style_guide().input_file(str(file))

    assert report.total_errors == 1
    assert len(report.get_statistics(rule)) == 1


def test_maccabe_complexity(tmpdir):
    content = """\
        def func(x):
            if x == 1:
                return 1
            elif x == 2:
                return 1
            elif x == 3:
                return 1
            elif x == 4:
                return 1
            elif x == 5:
                return 1
            elif x == 6:
                return 1
            elif x == 7:
                return 1
            elif x == 8:
                return 1
            elif x == 9:
                return 1
            elif x == 10:
                return 1
    """
    file = tmpdir.join("file.py").ensure()
    file.write(dedent(content))
    rule = "C901"

    report = flake8.get_style_guide().input_file(str(file))

    assert report.total_errors == 1
    assert len(report.get_statistics(rule)) == 1
