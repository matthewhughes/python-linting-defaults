from typing import Type

from mypy.options import Options
from mypy.plugin import Plugin


class DefaultOptionsPlugin(Plugin):
    def __init__(self, options: Options) -> None:
        options.disallow_untyped_defs = False
        options.disallow_untyped_calls = False
        options.disallow_untyped_decorators = False
        options.disallow_incomplete_defs = False
        options.strict_optional = False
        super().__init__(options)


def plugin(version) -> Type[Plugin]:
    return DefaultOptionsPlugin
