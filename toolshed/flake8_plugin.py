from argparse import Namespace
from ast import AST
from typing import Any, Generator, Tuple, Type

from flake8.formatting import base
from mccabe import McCabeChecker

SELECTS = {
    # builtin 
    "E",
    "W",
    "F", 
    # macabe: Function is too complex
    "C90",
}
IGNORES = {
    # e.g. black would format to: some_list[start + 4 :]
    "E203",
    # Line too long, let black handle line lengths
    "E501",
}
MAX_COMPLEXITY = 10
DEFUALT_COMPLEXITY = -1

class Plugin(base.BaseFormatter):
    name = __name__
    version = "0.0.1"

    def __init__(self, tree: AST):
        pass

    def run(self) -> Generator[Tuple[int, int, str, Type[Any]], None, None]:
        # Stop our plugin from actually doing any linting
        yield from []

    @classmethod
    def parse_options(cls, options: Namespace) -> None:
        options.extend_select = list(SELECTS.union(options.extend_select or []))
        options.extend_ignore = list(IGNORES.union(options.extend_ignore or []))
        
        # dirty, implementation dependant, hack
        # mccabe sets this value after parsing its options
        # and the order in which options are parsed depends on discovery order
        # which is a Python implementation detail
        max_complexity = McCabeChecker.max_complexity
        if max_complexity == DEFUALT_COMPLEXITY:
            McCabeChecker.max_complexity = MAX_COMPLEXITY
